-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Sun, 01 Jul 2017 19:48:54 +0200

For a number of reasons, i've recently set up a new OpenPGP key, and
will be transitioning away from my old one.

The old key will continue to be valid for some time, but i prefer all
future correspondence to come to the new one. I would also like this
new key to be re-integrated into the web of trust. This message is
signed by both keys to certify the transition.

The old key was:

pub   rsa4096 2013-11-17 [SC] [expires: 2018-03-31]
      ACB7 B67F 197F 9B32 1533  431C AC90 AC3E C524 065E
uid           [ultimate] Hugo Lefeuvre <hle@debian.org>
uid           [ultimate] Hugo Lefeuvre (hugo6390) <hugo6390@orange.fr>
uid           [ultimate] Hugo Lefeuvre <hle@owl.eu.com>
sub   rsa4096 2013-11-17 [E] [expires: 2018-03-31]
sub   rsa4096 2015-09-22 [A] [expires: 2018-03-31]

And the new key is:

pub   rsa4096 2017-06-30 [SC] [expires: 2018-06-30]
      9C4F C8BF A4B0 8FC5 48EB  56B8 1962 765B B9A8 BACA
uid           [  full  ] Hugo Lefeuvre <hle@owl.eu.com>
uid           [  full  ] Hugo Lefeuvre <hle@debian.org>
sub   rsa4096 2017-06-30 [E] [expires: 2018-06-30]
sub   rsa2048 2017-06-30 [S] [expires: 2018-06-30]
sub   rsa2048 2017-06-30 [A] [expires: 2018-06-30]

To fetch my new key from a public key server, you can simply do:

  gpg --keyserver pgp.mit.edu --recv-key 1962765BB9A8BACA 

If you already know my old key, you can now verify that the new key is
signed by the old one:

  gpg --check-sigs 1962765BB9A8BACA

If you don't already know my old key, you can check the fingerprint
against the one above:

  gpg --fingerprint AC90AC3EC524065E

If you are satisfied that you've got the right key, and the UIDs match
what you expect, I'd appreciate it if you would sign my key:

  gpg --sign-key 1962765BB9A8BACA

Lastly, if you could upload these signatures, i would appreciate it.
You can either send me an e-mail with the new signatures (if you have
a functional MTA on your system):

  gpg --armor --export 1962765BB9A8BACA | mail -s 'OpenPGP Signatures' hle@owl.eu.com

Or you can just upload the signatures to a public keyserver directly:

  gpg --keyserver pgp.mit.edu --send-key 1962765BB9A8BACA

Alternatively you could use the excellent caff[0] tool.

Please let me know if there is any trouble, and sorry for the
inconvenience.

This transition message is based on the excellent transition template
by dkg <dkg@fifthhorseman.net>. Thanks to him ! :)

Regards,

Hugo Lefeuvre (hle)

[0] https://wiki.debian.org/caff
-----BEGIN PGP SIGNATURE-----

iQEzBAEBCAAdFiEE5LpPtQuYJzvmooL3LVy48vb3khkFAllX6J0ACgkQLVy48vb3
khn/Xgf+Jp5TGlcp//WBrJBb98kziFIYr52tpndp5mRmwnuBb0WieX1FaovPVO2T
vbWFFbmbuIAGbNJ5yIVRo/uozZQIN0E3bDvzGSuO28nngzRUnlUdWy9ojI7UnOVd
cA1hSq3QcgsMlPyQ7lcjzWgWtLNb1Ej1jZHF8ecGPXjmMrrv+mtzD0AxXKAYG4G0
mLorr5xgkOAj0Un1edNpiZXjwo0lVuukoxgBMw4X9AXHc6fhKex9VrX1BDXIZMV6
LHbeuMJB9JMayVBqzfU5eA0E13S0qtZsaAlOFTFbSLjGq/5sBL/cuD4OyMcKibyC
NoNfomwXoQnCNgZGHM2ro6hUWv0wkYkCMwQBAQgAHRYhBKy3tn8Zf5syFTNDHKyQ
rD7FJAZeBQJZV+iqAAoJEKyQrD7FJAZex7UP/Auew/StYWowfaQ7yWO3gD2/rLUZ
f/FuXlj6doYjBAQGKHffhqIa3oBnvoNYU/LxRoW6rtMfVvXFIexnEacDjNItA4HT
FdTBfJ2YWIQyOWmaLBC8QImonBfQuINsVVZy3sCRMZxxuRodWXHIyl99QwdcldjA
rGeFgZ841Nx6mDnNIlxFZi0RNLzC1wUPBj9sDuJJqe/5LF+j3Z7dEnKdu/xr+BP7
FKrUi4XbywQal15utQdCuCMQiYX34vbn1CdXpsr/Rqqfx0Kl3DdCrqHBnzl5fmsh
ZmXTTYUugePNuSdbr1IJ9PgGMPOH6oKNqc44Ax6qB3GHnoaNJPrZfilvjyApbIEf
VaNTClSSvOFwcqJJUc3BoAUgAE6rClHBTcdZ4RXuWK11j6WG2GC3/RMRh/obQhn0
bXkO5f5cjeIpTXKYfADrqhWlWThpCeXqBv+tjmQTVD4Ki7P9olFu/nvlb8vzE+zQ
Zm0eSAVXNRm8/oHeVsVclaKR33w0k9qj5eiHcNsY5Nw06UuD2NNyjkALJRQMdqMr
+ZjamajZpOolJooJsRhUOJlgtq09/Fm+r8DAbYFZAptKnZ374d/TwcMI2cBWBo1V
qPWObcc+xoTwj3wuK70r4CRuoW2m6ljZuOQTHENNhj4L/suliwJqISlp/E2byFR+
RCmGKKtrybztKyYi
=mOL5
-----END PGP SIGNATURE-----
