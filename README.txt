                         .:/oo+++/-.`   
                       :os+/-.``.-/os+. 
                     `oo-`          -ss-
                    .s/      --.``   :o.
                    /o      -        .s-
                    +:     --     `  -o`
                    +:     ./   `  `:/` 
                    /s`     `-:.-:::`   
                    -ss-       ``       
                     -so.               
                      `/s/.             
                         .::-.`         


   Welcome to Hugo Lefeuvre's people.debian.org webpage.

# NEWS

[25/07/23] Updated expired keys.

# ABOUT ME

I'm a Debian Developer since 2015. Most of my Debian work is related to
packaging and security. I maintain a few Python modules, applications and
shared libraries. Among others bleachbit, openjpeg2, dlib or pyzor...  You can
find an exhaustive list here¹. I am also a paid contributor of the Debian LTS
team.

What you'll find here: Mostly test packages, documentation about sponsoring,
various information about my Debian involvement, etc. For more general
information please take a look at my personal website²!

Happy hacking!

[1] https://qa.debian.org/developer.php?login=hle%40debian.org
[2] https://www.owl.eu.com
